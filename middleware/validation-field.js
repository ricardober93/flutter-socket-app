const { validationResult } = require("express-validator");

const validationField = (req, res, next) => {
  const error = validationResult(req);

  if (!error.isEmpty()) {
    return res.status(400).json({
      ok: false,
      msg: error,
    });
  }

  next();
};

module.exports = {
  validationField,
};
