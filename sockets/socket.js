const {
  activeUser,
  inactiveUser,
  saveMessage,
} = require("../controllers/socket");
const { checkJWT } = require("../helpers/jwt");
const { io } = require("../index");
const { v4: uuid } = require("uuid");

io.on("connection", (client) => {
  console.log("Client connected");

  console.log(client.handshake.headers["x-token"]);

  const token = client.handshake.headers["x-token"];

  const [valido, uid] = checkJWT(token);

  if (!valido) {
    return client.disconnect();
  }

  activeUser(uid);

  //unir a la persona a una sala en particular
  client.join(uid);

  client.on("private-message", async (payload) => {
    await saveMessage(payload);

    io.to(payload.to).emit("private-message", payload);
  }),
    client.on("disconnect", (data) => {
      inactiveUser(uid);
      console.log("Client disconnected");
    });
});
