// path: api/messages

const { Router } = require("express");
const { validateJWT } = require("../middleware/validateJWT");
const { getMessages } = require("../controllers/message");
const router = Router();

router.get("/:from", validateJWT, getMessages);

module.exports = router;
