// path: api/login

const { Router } = require("express");
const { createUser, loginUser, reNew } = require("../controllers/auth");
const { check } = require("express-validator");
const { validationField } = require("../middleware/validation-field");
const { validateJWT } = require("../middleware/validateJWT");

const router = Router();

router.post(
  "/",
  [
    check("password", "El password debe ser obligatior").not().isEmpty(),
    check("email", "El email debe ser obligatior").not().isEmpty().isEmail(),
    validationField,
  ],
  loginUser
);
router.post(
  "/new",
  [
    check("name", "El name debe ser obligatior").not().isEmpty(),
    check("password", "El password debe ser obligatior").not().isEmpty(),
    check("email", "El email debe ser obligatior").not().isEmpty().isEmail(),
    validationField,
  ],
  createUser
);

router.get("/renew", validateJWT, reNew);

module.exports = router;
