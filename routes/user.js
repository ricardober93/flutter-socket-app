// path: api/user

const { Router } = require("express");
const { validateJWT } = require("../middleware/validateJWT");
const { getUser } = require("../controllers/user");
const router = Router();

router.get("/users", validateJWT, getUser);

module.exports = router;
