const Message = require("../models/Message");
const User = require("../models/user");

const activeUser = async (uid = "") => {
  const user = await User.findById(uid);
  if (!user) {
    return res.status(404).json({
      ok: false,
      msg: "Usuario no existe",
    });
  }
  user.online = true;
  await user.save();
  return user;
};

const inactiveUser = async (uid = "") => {
  const user = await User.findById(uid);
  if (!user) {
    return res.status(404).json({
      ok: false,
      msg: "Usuario no existe",
    });
  }
  user.online = false;
  await user.save();
  return user;
};

const saveMessage = async (payload) => {
  console.log("🚀 ~ file: socket.js:31 ~ saveMessage ~ payload:", payload);
  // payload.from, payload.to, payload.message
  try {
    const message = new Message(payload);
    console.log("🚀 ~ file: socket.js:35 ~ saveMessage ~ message:", message);
    await message.save();
    return true;
  } catch (error) {
    return false;
  }
};

module.exports = {
  activeUser,
  inactiveUser,
  saveMessage,
};
