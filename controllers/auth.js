const bcrypt = require("bcryptjs/dist/bcrypt");
const User = require("../models/user");
const { generateJWT } = require("../helpers/jwt");

const createUser = async (req, res) => {
  const { email, password, name } = req.body;

  try {
    const existEmail = await User.findOne({ email });

    if (existEmail) {
      return res.status(500).json({
        ok: false,
        msg: "Existe un usuario con ese email",
      });
    }

    const salt = bcrypt.genSaltSync();

    const user = new User({ name, email, password });
    user.password = bcrypt.hashSync(password, salt);

    //generar JTW
    const token = await generateJWT(user.id);

    await user.save();
    res.json({
      ok: true,
      user,
      token,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      ok: false,
      msg: "Error inesperado",
    });
  }
};

const loginUser = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(404).json({
        ok: false,
        msg: "Usuario o password invalidos",
      });
    }
    const validatePassword = bcrypt.compareSync(password, user.password);

    if (!validatePassword) {
      return res.status(400).json({
        ok: false,
        msg: "password invalido",
      });
    }
    //generar JTW
    const token = await generateJWT(user.id);

    res.json({
      ok: true,
      user,
      token,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      ok: false,
      msg: "Error inesperado",
    });
  }
};

const reNew = async (req, res) => {
  const { uid } = req;

  const user = await User.findById(uid);

  const token = await generateJWT(uid);
  res.json({
    ok: true,
    user,
    token,
  });
};

module.exports = {
  createUser,
  loginUser,
  reNew,
};
