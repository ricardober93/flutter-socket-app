const Messages = require("../models/Message");

const getMessages = async (req, res) => {
  const messages = await Messages.find({
    $or: [
      { from: req.uid, to: req.params.from },
      { from: req.params.from, to: req.uid },
    ],
  })
    .sort({ createdAt: -1 })
    .limit(30);

  res.json({
    ok: true,
    messages,
  });
};
module.exports = {
  getMessages,
};
