const user = require("../models/user");

const getUser = async (req, res) => {
  const desde = Number(req.query.desde) || 0;
  const limit = Number(req.query.limit) || 5;

  const users = await user
    .find({
      _id: {
        $ne: req.uid,
      },
    })
    .skip(desde)
    .limit(5)
    .sort({ online: -1 });

  res.json({
    ok: true,
    users,
    desde,
    limit,
  });
};

module.exports = {
  getUser,
};
