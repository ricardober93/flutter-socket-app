const jsonwebtoken = require("jsonwebtoken");

const generateJWT = (uid = "") => {
  const payload = { uid };
  return new Promise((resolve, reject) => {
    jsonwebtoken.sign(
      payload,
      process.env.SECRET,
      {
        expiresIn: "48h",
      },
      (err, token) => {
        if (err) {
          console.log(err);
          reject("Cant generate token");
        } else {
          resolve(token);
        }
      }
    );
  });
};


const checkJWT = (token = "") => {
  try {
    const { uid } = jsonwebtoken.verify(token, process.env.SECRET);
    return [true, uid];
  } catch (error) {
    return [false, null];
  }
};

module.exports = {
  generateJWT,
  checkJWT,
};
