const express = require("express");
const path = require("path");
const { createServer } = require("node:http");
const { Server } = require("socket.io");
require("dotenv").config();

require("./database/config").dbConnetion();

const app = express();
const server = createServer(app);
module.exports.io = new Server(server);

const publicPath = path.resolve(__dirname, "public");

require("./sockets/socket");

app.use(express.static(publicPath));
app.use(express.json()); // parsea el body

//routes
app.use("/api/login", require("./routes/auth"));

app.use("/api", require("./routes/user"));

app.use("/api/messages", require("./routes/messages"));
server.listen(process.env.PORT || 3000, (err) => {
  if (err) {
    console.log("Error: ", err);
  } else {
    console.log("Server is listening on port 3000");
  }
});
